# go-mux

The go-mux package implements a radix tree based request multiplexer. It is fully compatible with the
`net/http` package.

> go-mux requires at leats Go v1.7

## License

This library is distributed under the BSD-style license found in the [LICENSE](./LICENSE) file.

